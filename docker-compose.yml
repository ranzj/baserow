version: "3.7"

services:
  db:
    container_name: db
    image: postgres:11.3
    environment:
      - POSTGRES_USER=${DATABASE_USER:-baserow}
      - POSTGRES_PASSWORD=${DATABASE_PASSWORD:-baserow}
      - POSTGRES_DB=${DATABASE_NAME:-baserow}
    ports:
      - "${POSTGRES_PORT:-5432}:5432"
    networks:
      local:
    volumes:
      - pgdata:/var/lib/postgresql/data

  redis:
    container_name: redis
    image: redis:6.0
    ports:
      - "${REDIS_PORT:-6379}:6379"
    networks:
      local:

  mjml:
    container_name: mjml
    image: liminspace/mjml-tcpserver:0.10
    # mjml is based off the node image which creates a non root node user we can run as
    user: "1000:1000"
    ports:
      - "${MJML_PORT:-28101}:28101"
    networks:
      local:

  backend:
    container_name: backend
    build:
      dockerfile: ./backend/Dockerfile
      context: .
    image: baserow_backend:latest
    environment:
      - MIGRATE_ON_STARTUP=${MIGRATE_ON_STARTUP:-true}
      - SYNC_TEMPLATES_ON_STARTUP=${SYNC_TEMPLATES_ON_STARTUP:-true}
      - DATABASE_USER=${DATABASE_USER:-baserow}
      - DATABASE_PASSWORD=${DATABASE_PASSWORD:-baserow}
      - DATABASE_NAME=${DATABASE_NAME:-baserow}
      - ADDITIONAL_APPS
      - MEDIA_URL=http://localhost:${MEDIA_PORT:-4000}/media/
    ports:
      - "${BACKEND_PORT:-8000}:8000"
    depends_on:
      - db
      - redis
      - mjml
      - media-volume-fixer
    volumes:
      - media:/baserow/media
    networks:
      local:

  celery:
    container_name: celery
    image: baserow_backend:latest
    command: celery
    depends_on:
      - backend
    volumes:
      - media:/baserow/media
    networks:
      local:

  web-frontend:
    container_name: web-frontend
    build:
      context: .
      dockerfile: ./web-frontend/Dockerfile
    image: baserow_web-frontend:latest
    environment:
      - ADDITIONAL_MODULES
    ports:
      - "${WEB_FRONTEND_PORT:-3000}:3000"
    depends_on:
      - backend
    networks:
      local:

  # A nginx container purely to serve up django's MEDIA files.
  media:
    container_name: media
    build: media
    ports:
      - "${MEDIA_PORT:-4000}:80"
    depends_on:
      - media-volume-fixer
    volumes:
      - media:/baserow/media
    networks:
      local:

  # When switching between dev and local the media files in the media volume will be
  # owned by different users. Ensure that we chown them to the user appropriate for the
  # environment here.
  media-volume-fixer:
    container_name: media-volume-fixer
    image: bash:4.4
    command: chown 9999:9999 -R /baserow/media
    volumes:
      - media:/baserow/media
    networks:
      local:

volumes:
  pgdata:
  media:

networks:
  local:
    driver: bridge
